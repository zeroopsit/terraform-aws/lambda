################ permissions ################
resource "aws_lambda_permission" "lambda" {
    count = length(var.trigger_permissions)
    action        = "lambda:InvokeFunction"
    function_name = aws_lambda_function.lambda.function_name
    principal     = "${lookup(var.trigger_permissions[count.index], "principal")}.amazonaws.com"
    source_arn    = lookup(var.trigger_permissions[count.index], "source_arn")
}