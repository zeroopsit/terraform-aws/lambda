##################### PERMISSIONS #####################
variable "trigger_permissions" {
  description = "The list of services that need to have permissions to trigger the lambda"
  type        = "list"
}

variable "lambda_permissions" {
  description = "The filename with path of the deployment package for lambda"
  type        = string
}

##################### LAMBDA CONFIG #####################
variable "function_name" {
  description = "The name of the lambda function"
  type        = string
}

variable "handler" {
  description = "The function handler"
  type        = string
}

variable "runtime" {
  description = "The function handler"
  type        = string
}

variable "filename" {
  description = "The filename with path of the deployment package for lambda"
  type        = string
}

variable "environment_variables" {
  description = "A map of the environment variables for the lambda function"
  type        = "map"
}