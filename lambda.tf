################ LAMBDA ################
resource "aws_lambda_function" "lambda" {
    filename      = var.filename
    function_name = var.function_name
    role          = aws_iam_role.lambda.arn
    handler       = var.handler
    runtime       = var.runtime

    source_code_hash = filebase64sha256(var.filename)

    environment {
        variables = var.environment_variables
    }
}

