################ LAMBDA ROLE ################
resource "aws_iam_role" "lambda" {
    name                = "LAMBDA-${upper(var.function_name)}"
    assume_role_policy  = data.aws_iam_policy_document.assume.json
}

data "aws_iam_policy_document" "assume" {
  statement {
    actions     = ["sts:AssumeRole"]
    effect      = "Allow"
    principals  { 
        type        = "Service" 
        identifiers = ["lambda.amazonaws.com"]
    }
  }
}

################ LAMBDA DEFAULT PERMISSIONS ################
resource "aws_iam_role_policy" "default" {
    name   = "default_permissions"
    role   = aws_iam_role.lambda.name
    policy = data.aws_iam_policy_document.default.json
}

data "aws_iam_policy_document" "default" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
}

################ LAMBDA CUSTOM PERMISSIONS ################
resource "aws_iam_role_policy" "lambda" {
    name   = "custom_permissions"
    role   = aws_iam_role.lambda.name
    policy = var.lambda_permissions
}